# @summary Cassandra Reeaper configuration
#
# Configuration of Cassandra Reeaper based
# on configs provided in main class
#
# @example
#  it's included in main class, don't use directly 
class bdi_cassandra_reaper::config {
  if $bdi_cassandra_reaper::owner {
    $system_user = $bdi_cassandra_reaper::owner
  } else {
    $system_user = 'reaper'
  }

  if $bdi_cassandra_reaper::group {
    $system_group = $bdi_cassandra_reaper::group
  } else {
    $system_group = 'reaper'
  }

  if $bdi_cassandra_reaper::use_ssl {
    file { "${bdi_cassandra_reaper::config_dir}/${bdi_cassandra_reaper::ssl_properties}":
      ensure  => file,
      owner   => 'root',
      group   => $system_group,
      mode    => '0640',
      content => Sensitive(epp("${module_name}/cassandra-reaper-ssl.properties.epp", {
            use_ssl               => $bdi_cassandra_reaper::use_ssl,
            jmx_client_auth       => $bdi_cassandra_reaper::jmx_client_auth,
            jmx_keystore          => $bdi_cassandra_reaper::jmx_keystore,
            jmx_truststore        => $bdi_cassandra_reaper::jmx_truststore,
            jmx_keystore_phrase   => $bdi_cassandra_reaper::jmx_keystore_phrase,
            jmx_truststore_phrase => $bdi_cassandra_reaper::jmx_truststore_phrase,
      })),
      notify  => Service['cassandra-reaper'],
    }
  }
  if $bdi_cassandra_reaper::shiro_ini or $bdi_cassandra_reaper::shiro_use_sha256 {
    if $bdi_cassandra_reaper::shiro_use_sha256 {
      $shiro_clear = deep_merge(lookup('bdi_cassandra_reaper::default_shiro_config'),$bdi_cassandra_reaper::shiro_config)
      $users = $shiro_clear['users'].reduce({}) | $memo, $x | {
        $spr=split($x[1],',')
        $p256 = $spr[0].sha256()
        $rd = $spr[1]
        $memo + {
        $x[0] => "${p256},${rd}" }
      }
      $shiro_sha256 = {
        'main'                        => {
          'sha256matcher'               => 'org.apache.shiro.authc.credential.Sha256CredentialsMatcher',
          'iniRealm.credentialsMatcher' => '$sha256matcher',
        },
        'users'                       => $users,
      }
    } else {
      $shiro_sha256 = undef
    }
    $shiro = deep_merge(lookup('bdi_cassandra_reaper::default_shiro_config'),$bdi_cassandra_reaper::shiro_config, $shiro_sha256 ,)
    $ini_settings_defaults = {
      'path'    => "${bdi_cassandra_reaper::config_dir}/${bdi_cassandra_reaper::shiro_ini}",
      'require' => Package['reaper'],
      'notify'  => Service['cassandra-reaper'],
    }
    inifile::create_ini_settings($shiro, $ini_settings_defaults)
    file { "${module_name}_${name}_${bdi_cassandra_reaper::config_dir}/${bdi_cassandra_reaper::shiro_ini}":
      ensure => 'file',
      name   => "${bdi_cassandra_reaper::config_dir}/${bdi_cassandra_reaper::shiro_ini}",
      owner  => 'root',
      group  => $system_group,
      mode   => '0640',
    }
  }
  if $bdi_cassandra_reaper::reaper_config {
    if $bdi_cassandra_reaper::shiro_ini {
      $shiro_reaper_hash = {
        'accessControl' => {
          'shiro' => {
            'iniConfigs' => ["file:${bdi_cassandra_reaper::config_dir}/${bdi_cassandra_reaper::shiro_ini}",],
          },
        },
      }
    }
    if $bdi_cassandra_reaper::storage_config {
      $storage_config_no_ssl = {
        'cassandra'   => {
          'clusterName'         => $bdi_cassandra_reaper::storage_config['clustername'],
          'keyspace'            => $bdi_cassandra_reaper::storage_config['keyspace'],
          'contactPoints'       => $bdi_cassandra_reaper::storage_config['contactPoints'],
          'loadBalancingPolicy' => {
            'type'            => 'tokenAware',
            'shuffleReplicas' => true,
            'subPolicy'       => {
              'type'                                   => 'dcAwareRoundRobin',
              #              'localDC'                                => nill,
              'usedHostsPerRemoteDC'                   => 0,
              'allowRemoteDCsForLocalConsistencyLevel' => false,
            },
          },
          'authProvider'        => {
            'type'     => 'plainText',
            'username' => $bdi_cassandra_reaper::storage_config['username'],
            'password' => $bdi_cassandra_reaper::storage_config['password'],
          },
        },
        'logging'     => {
          'loggers' => {
            'com.datastax.driver.core.QueryLogger.NORMAL' => {
              'level'     => 'DEBUG',
              'additive'  => false,
              'appenders' => [
                'type'                       => 'file',
                'currentLogFilename'         => '/var/log/cassandra-reaper/query-logger.log',
                'archivedLogFilenamePattern' => 'query-logger-%d.log.gz',
                'archivedFileCount'          => 2,
              ],
            },
          },
        },
        'storageType' => 'cassandra',
      }
      if $bdi_cassandra_reaper::use_ssl {
        $storage_config_ssl = {
          'cassandra' => {
            'ssl' => {
              'type' => 'jdk',
            },
          },
        }
      } else {
        $storage_config_ssl = {}
      }
      $storage_config = deep_merge($storage_config_no_ssl,$storage_config_ssl)
    } else {
      $storage_config = {}
    }
    if $bdi_cassandra_reaper::ui_keystore
    and $bdi_cassandra_reaper::ui_truststore
    and $bdi_cassandra_reaper::ui_keystore_phrase
    and $bdi_cassandra_reaper::ui_truststore_phrase {
      $ui = {
        'server' => {
          'applicationConnectors' => [
            'type'               => 'https',
            'port'               => '8443',
            'bindHost'           => '0.0.0.0',
            'keyStorePath'       => $bdi_cassandra_reaper::ui_keystore,
            'keyStorePassword'   => $bdi_cassandra_reaper::ui_keystore_phrase,
            'trustStorePath'     => $bdi_cassandra_reaper::ui_truststore,
            'trustStorePassword' => $bdi_cassandra_reaper::ui_truststore_phrase,
            'validateCerts'      => false,
          ],
        },
      }
    } else {
      $ui = {}
    }

    if $bdi_cassandra_reaper::system_property {
      $cryptograph = {
        'cryptograph' => {
          'type'                 => 'symmetric',
          'systemPropertySecret' => $bdi_cassandra_reaper::system_property['key'],
        },
      }
    } else {
      $cryptograph = {}
    }

    if $facts['systemd'] {
      if $bdi_cassandra_reaper::system_property
      or $bdi_cassandra_reaper::owner
      or $bdi_cassandra_reaper::group
      or $bdi_cassandra_reaper::use_ssl {
        systemd::dropin_file { "${module_name}_${name}_override.conf":
          filename => 'override.conf',
          owner    => 'root',
          group    => 'root',
          mode     => '0640',
          unit     => 'cassandra-reaper.service',
          content  => Sensitive(epp("${module_name}/cassandra-reaper-dropin.epp", {
                system_property       => $bdi_cassandra_reaper::system_property['key'],
                system_property_value => $bdi_cassandra_reaper::system_property['value'],
                owner                 => $bdi_cassandra_reaper::owner,
                group                 => $bdi_cassandra_reaper::group,
                use_ssl               => $bdi_cassandra_reaper::use_ssl,
          })),
        }
      }
    } else {
      notify { "${module_name}::${name}":
        message => "Only systemd is supported, some startup settings haven't been set",
      }
    }

    $reaper = deep_merge(
      lookup('bdi_cassandra_reaper::default_reaper_config'),
      $bdi_cassandra_reaper::reaper_config,
      $shiro_reaper_hash,
      $storage_config,
      $ui,
      $cryptograph,
    )

    file { "${bdi_cassandra_reaper::config_dir}/cassandra-reaper.yaml":
      ensure  => file,
      owner   => 'root',
      group   => $system_group,
      mode    => '0640',
      content => Sensitive(to_yaml($reaper)),
      notify  => Service['cassandra-reaper'],
    }
  }
}
