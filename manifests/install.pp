# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include bdi_cassandra_reaper::install
class bdi_cassandra_reaper::install {
  if $bdi_cassandra_reaper::manage_repos {
    case $facts['os']['family'] {
      'Debian': {
        include bdi_cassandra_reaper::debian
      }
      'RedHat': {
        include bdi_cassandra_reaper::redhat
      }
      default: {
        notify { 'Cassandra Reaper':
          message => "Unsupported OS family: ${facts['os']['family']}",
        }
      }
    }
  }
  if $bdi_cassandra_reaper::manage_jdk {
    package { "${module_name}::${bdi_cassandra_reaper::jdk_package}":
      ensure => installed,
      name   => $bdi_cassandra_reaper::jdk_package,
    }
  }
  package { 'reaper':
    ensure => installed,
  }
}
