# @summary module install and configure Cassandra Reaper
#
# main class and only it should be included to work properly
# no other classese are required
#
# @example
#   include bdi_cassandra_reaper
#
# @param service_ensure
# set service status after installation
# @see https://www.puppet.com/docs/puppet/7/types/service.html#service-attributes
#
# @param service_enable
# enable/disable service on boot time
#
# @param manage_repos
# enable managing repos (apt/yum) for cassandra reaper
#
# @param use_ssl
#  enable ssl to jmx/cassandra db connections
# 
# @param jmx_client_auth
# enable jmx authorization of jmx clients (required if jmx auth is enabled on cassandra db side),
# it requires jmx client certificates which are required
# 
# @param ssl_properties
# ssl properties file name
#
# @param config_dir
# cassandra config dir (full path)
#
# @param owner
# different user to run reaper than default
#
# @param group
# different group to run reaper than default
#
# @param system_property
# to store secrets in backend (cassandra) it has to be configured
# simply hash key   => varable name
#             value => shared secret
#
# @param shiro_ini
# shiro.ini's config file name
#
# @param shiro_use_sha256
# enables shiro's non clear text passwords (sha256 hash)
#
# @param shiro_config
# hash contain non default values for shiro.ini
#
# @param jmx_keystore
# path for jmx keystore (required if jmx_client_auth)
#
# @param jmx_truststore
# jks truststore for jks
#
# @param jmx_keystore_phrase
# phrase for jmx_keystore
#
# @param jmx_truststore_phrase
# phrase for jmx_truststore
#
# @param storage_config
# simply hash which contain cassandra storage config
# it has precedence over reaper_config (if save keys are in
# reaper_config and storage config storage_config is more important)
#
# @param ui_keystore
# jks key store which enables ssl on webui
# on dedicated port 8443
#
# @param ui_truststore
# jks trustore for ui
#
# @param ui_keystore_phrase
# pharse for ui_keystore
#
# @param ui_truststore_phrase
# phrase for ui_truststore
#
# @param reaper_config
# full or partial reaper config hash
#
# @param repos
# hash which contain repos data for 
# particular OS family version etc
#
# @param manage_jdk
# enable managing jdk by module
# by default java 11 jdk nohead
# is installed
#
# @param jdk_package
# non default jdk package
#
class bdi_cassandra_reaper (
  Enum['stopped','running','false','true'] $service_ensure = 'running',
  Boolean $service_enable = true,
  Boolean $manage_repos = true,
  Boolean $use_ssl = false,
  Boolean $jmx_client_auth = false,
  Optional[String] $ssl_properties = undef,
  Optional[Hash] $storage_config = undef,
  Optional[Stdlib::Absolutepath] $config_dir = undef,
  Optional[String] $owner = undef,
  Optional[String] $group = undef,
  Optional[Hash] $system_property = undef,
  Optional[String] $shiro_ini = undef,
  Optional[Boolean] $shiro_use_sha256 = undef,
  Optional[Hash] $shiro_config = undef,
  Optional[Stdlib::Absolutepath] $jmx_keystore = undef,
  Optional[Stdlib::Absolutepath] $jmx_truststore = undef,
  Optional[String] $jmx_keystore_phrase = undef,
  Optional[String] $jmx_truststore_phrase = undef,
  Optional[Stdlib::Absolutepath] $ui_keystore = undef,
  Optional[Stdlib::Absolutepath] $ui_truststore = undef,
  Optional[String] $ui_keystore_phrase = undef,
  Optional[String] $ui_truststore_phrase = undef,
  Optional[Hash] $reaper_config = undef,
  Optional[Hash] $repos = undef,
  Optional[Boolean] $manage_jdk = undef,
  Optional[String] $jdk_package = undef,
) {
  contain bdi_cassandra_reaper::install
  contain bdi_cassandra_reaper::config
  contain bdi_cassandra_reaper::service

  Class['bdi_cassandra_reaper::install']
  -> Class['bdi_cassandra_reaper::config']
  -> Class['bdi_cassandra_reaper::service']
}
