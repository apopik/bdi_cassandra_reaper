# @summary Manage debian repos
#
# class manage debian repos gpg keys and required packages 
#
# @example
#  it's included in main class, don't use directly
class bdi_cassandra_reaper::debian {
  package { "${module_name}_apt-transport-https":
    ensure => installed,
    name   => 'apt-transport-https',
  }

  $bdi_cassandra_reaper::repos.each | String $name, Hash $repo | {
    file { "${name}_${$repo['gpg_key']}":
      ensure => file,
      name   => "/etc/apt/trusted.gpg.d/${repo['gpg_key']}",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "puppet:///modules/${module_name}/${repo['gpg_key']}",
      notify => Exec['apt_update'],
    }
    apt::source { $name:
      comment  => $repo['name'],
      location => "[signed-by=/etc/apt/trusted.gpg.d/${repo['gpg_key']}] ${repo['location']}",
      release  => $facts['os']['distro']['codename'],
      repos    => 'main',
      require  => [File["${name}_${$repo['gpg_key']}"], Package["${module_name}_apt-transport-https"]],
      before   => Package['reaper'],
    }
  }
}
