# @summary Red Hat repos
#
# manage Red Hat family OS yum repos
#
# @example
#  it's included in main class, don't use directly 
class bdi_cassandra_reaper::redhat {
  $bdi_cassandra_reaper::repos.each | String $name, Hash $repo | {
    yumrepo { $name:
      ensure          => present,
      name            => $name,
      baseurl         => $repo['baseurl'],
      enabled         => $repo['enabled'],
      gpgcheck        => $repo['gpgcheck'],
      gpgkey          => $repo['gpgkey'],
      metadata_expire => $repo['metadata_expire'],
      sslcacert       => $repo['sslcacert'],
      sslverify       => $repo['sslverify'],
    }
  }
}
