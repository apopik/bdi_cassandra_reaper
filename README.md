# BDI Cassandra Reaper

Welcome to BDI cassandra reaper module.


## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with bdi cassandra reaper](#setup)
    * [What bdi cassandra reaper affects](#what-bdi_cassandra_reaper-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with bdi cassandra reaper](#beginning-with-bdi_cassandra_reaper)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module install and configure cassandra [reaper](http://cassandra-reaper.io/) package on RHEL and Debian
based systems.

## Setup

### What BDI Cassandra Reaper affects

The module manage folowing files:

* repo file (depends on rhel/debian machine, with gpg keys etc.)
* cassandra reaper package (version depends on local OS policy but usually latest, the module doesn't update reaper package)
* jdk package (optionally if there is no special requirements it can install jdk from OS repos)
* $ETCDIR/cassandra-reaper.yaml (reaper main configuration file)
* $ETCDIR/cassandra-reaper-ssl.properties
* $ETCDIR/shiro.ini (authentication/authorizatioon file)
* systemd override.conf for reaper
* systemd service

### Setup Requirements

The module requires additional puppet modules:

* puppetlabs/stdlib
* puppetlabs/inifile

### Beginning with BDI Cassandra Reaper

The easiest way to enable bdi cassandra module is just to include the module in your code:

```
include bdi_cassandra_reaper
```

## Usage

it will install cassandra reaper package with dependenties and try to start service,
like on example:

```
class { 'bdi_cassandra_reaper':
  service_ensure   => 'started',
  service_enable   => true,
  manage_jdk       => true,
  shiro_ini        => 'shiro.ini',
  reaper_config    => {
    'scheduleDaysBetween' => 11,
  },
  use_ssl          => true,
  shiro_use_sha256 => true,
}
```

ssl is enforced so TLS keystores have to be created.

The module doesn't manage any certificates, so if there any TLS is required java keystores have to be created by other module/code.

## Limitations

The module has been created to solve BDI requirements so probably it has some limmitations/bugs which were not required by BDI

## Development

If you think that module is usefeul for you, you can contribute this code and make changes, any patches are welcome :)

## Release Notes/Contributors/Etc.

First near-release that provides BDI-required features - but Warning may kill your dog, cat etc. - you've been warned :)

[1]: https://puppet.com/docs/pdk/latest/pdk_generating_modules.html
[2]: https://puppet.com/docs/puppet/latest/puppet_strings.html
[3]: https://puppet.com/docs/puppet/latest/puppet_strings_style.html
