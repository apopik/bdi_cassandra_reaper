#include bdi_cassandra_reaper
class { 'bdi_cassandra_reaper':
  service_ensure   => 'stopped',
  service_enable   => false,
  manage_jdk       => true,
  shiro_ini        => 'shiro.ini',
  reaper_config    => {
    'scheduleDaysBetween' => 11,
  },
  use_ssl          => true,
  shiro_use_sha256 => true,
}
